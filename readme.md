# ES.IoC

This is Enoki Solutions Inversion of Control library. We've released it under MIT License for anyone to use.

## Design

### Anit-Container, Anti-Configuration

This is **not** an IoC "Container". While there is a concept of an `ExecutionContext`, none of the components being wired are aware of it. Components should
only take a dependency on `ES.IoC.Wiring`. Each concrete component that is `[Wire]`d will have one, and only one, instance created when the applications starts. Service
interfaces can be implemented multiple times, and a component can ask for an array of all implementors of a service.  Wiring of components is fully
deterministic and non-conditional. This is **not** configuration, which is outside of the scope of these libraries and a different concern. In production mode the
`ExecutionContext` does not get created and is replaced by simple code that can be debugged instead of the reflection based code.

### Expections of Components
Components in this system are akin to components used in an electrical circuit. While components are being wired together they do not send signals down the wires
until all of them are assembled and the circuit is powered. In our case each constructor receives the components it needs and only records their existence in `private`
instance members before returning. While it may be tempting to call into a configuration component to record its answer only avoid doing this as it will limit how
the configuration service can work as the entire system is not yet setup at the time of the call. (This can lead to hard to find bugs.)

Each component implements an interface (generally only one interface as implementing more than one is usally a SRP violation). The interface it implements is how
other components will ask for it. Since we do no use reflection in production mode both the interface and implementing class must be `public` with a `public` contructor.
The class should also be `sealed` to avoid any attempts at inheritence.

### Expections of the Applications
An application consumes components and provides at least one main component of its own. Typically something like `IRun` with a `Run()` method in it. This is akin
to the power button for the circuit. The concrete implementation of `IRun` is like any other component and will form the root of the dependency graph used
to find the components needed for the application. The application's `Bootstrap.cs` code will return an instance of `IRun` and the `Main` method will call its
`Run` method.

How `Bootstrap.cs` creates the `IRun` component depends of the mode you're using. If you update your circuit design by adding more components or wires you'll
have to run in reflection based mode (i.e. don't define `ES_IOC_USE_BOOTSTRAP`). This will re-write the generated code in `Bootstrap.cs` used for final releases.

### `[Wire]` vs `[Wirer]`

`[Wire]` is used for a components directly. The `[Wirer]` is used to adapt interfaces. This is easiest to explain with an example. If you have a generalized
named end point that takes in parameters and returns a result you can use `[Wirer]` to adapt all the generalized endpoints to HTTP transport and another one to
adapt all of them to command line. It's basically a lambda function for converting one type into another and you likely won't need it unless you're getting fancy.

A `[Wirer]` can ask for instances the same way a component's constructor does and produce a single instance that implements an interface, or an array of instances 
that implement the same interface type. For example, you could take in an array of sources and an array of sinks are return the cross product of them as connected
pipes.

### Testing

`ES.IoC.Test` has examples of how to create tests using this system. `TestForTest` shows how to use `FakeItEasy` to create a component under test where all of the
services provided to it are `Fake`s. You can use any library you like to generate stub/fake implementations of interfaces. This pattern can be used to unit test any
component in isolation of its dependencies.
